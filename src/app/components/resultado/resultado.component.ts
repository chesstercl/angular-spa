import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {

  resultado: Heroe[] = [];
  parametro: string;
  constructor(
    private heroesService: HeroesService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {

      this.activatedRoute.params.subscribe ( params => {
        this.parametro = params.parametro;
        this.resultado = this.heroesService.buscarHeroes(params.parametro);
        console.log(this.resultado + 'resultado constructor');
    });
  }
  ngOnInit() {
/*     this.resultado = this.heroesService.buscarHeroes(parametro);
    console.log(this.resultado + 'resultado ngOnInit'); */
  }

verHeroe(idx: number) {
    this.router.navigate( ['/heroe', idx]);
  }
}

